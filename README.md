# Documentation
The documentation for how to use all of the Scout a Team services.
It also contains the API documentation, but the viewable version is behind an identity-aware proxy.
This is done because the API should be kept private.

**TODO:** add [Docusaurus](https://docusaurus.io/docs/en/installation) integration
