get:
  tags:
    - users
  security:
    - AuthenticationToken: []
  summary: Describe a user
  description: |
    Retrieve all a user's details on a given team.
  parameters:
    - in: path
      name: team
      schema:
        type: string
      required: true
      description: The ID of the team
    - in: path
      name: user
      schema:
        type: string
      required: true
      description: The ID of the user
  responses:
    200:
      description: Successfully read user details
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                description: Success status message (will always be "success")
                example: success
              data:
                $ref: '../models.yaml#/user'
    400:
      description: Invalid request headers, body, or parameters
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                description: Error status message (will always be "error")
                example: error
              reason:
                type: string
                description: Human readable reason for failure
                example: invalid user id format for path parameter 'user'
    401:
      description: Invalid API token
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                description: Error status message (will always be "error")
                example: error
              reason:
                type: string
                description: Human readable reason for failure
                example: "invalid token: failed to find signing key for token"
    404:
      description: Specified user or team does not exist
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                description: Error status message (will always be "error")
                example: error
              reason:
                type: string
                description: Human readable reason for failure
                example: "specified user does not exist"

put:
  tags:
    - users
  security:
    - AuthenticationToken: []
  summary: Update a user
  description: |
    Modify a user's details in the database. Either the user themself can
    update their information or a user with a role greater than or equal to 
    administrator.
  parameters:
    - in: path
      name: team
      schema:
        type: string
      required: true
      description: The ID of the team
    - in: path
      name: user
      schema:
        type: string
      required: true
      description: The ID of the user
  requestBody:
    description: Fields to be updated
    required: true
    content:
      application/json:
        schema:
          type: object
          properties:
            name:
              type: string
              description: The new name to set
              example: Not Alex Krantz
            email:
              type: string
              description: The new email to set
              example: not.alex@krantz.dev
            password:
              type: string
              description: The new password to set
              minLength: 64
              maxLength: 64
              example: 9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08
            role:
              type: string
              description: The new role to set
              example: administrator
            enabled:
              type: bool
              description: Whether to enable the user or not
              example: false
  responses:
    200:
      description: Successfully updated the user
      content:
        application/json:
          schema:
            $ref: '../models.yaml#/success'
    400:
      description: Invalid request headers, body, or parameters
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                description: Error status message (will always be "error")
                example: error
              reason:
                type: string
                description: Human readable reason for failure
                example: header 'Content-Type' must be 'application/json'
    401:
      description: Invalid API token
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                description: Error status message (will always be "error")
                example: error
              reason:
                type: string
                description: Human readable reason for failure
                example: "invalid token: failed to find signing key for token"
    404:
      description: Specified user or team does not exist
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                description: Error status message (will always be "error")
                example: error
              reason:
                type: string
                description: Human readable reason for failure
                example: "specified user does not exist"

delete:
  tags:
    - users
  security:
    - AuthenticationToken: []
  summary: Delete a user
  description: |
    Delete a user's account and disassociate them with the team. A user is not allowed
    to delete their account, a user with priviliges greater than or equal to administrator
    must do it for them. The owner is not allowed to delete their account, they must 
    delete the whole team or transfer ownership and then delete their account.
  parameters:
    - in: path
      name: team
      schema:
        type: string
      required: true
      description: The ID of the team
    - in: path
      name: user
      schema:
        type: string
      required: true
      description: the ID of the user
  responses:
    200:
      description: Successfully deleted user
      content:
        application/json:
          schema:
            $ref: '../models.yaml#/success'
    400:
      description: Invalid request headers, body, or parameters
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                description: Error status message (will always be "error")
                example: error
              reason:
                type: string
                description: Human readable reason for failure
                example: invalid team id format for path parameter 'team'
    401:
      description: Invalid API token
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                description: Error status message (will always be "error")
                example: error
              reason:
                type: string
                description: Human readable reason for failure
                example: "invalid token: failed to find signing key for token"
    404:
      description: Specified user or team does not exist
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                description: Error status message (will always be "error")
                example: error
              reason:
                type: string
                description: Human readable reason for failure
                example: "specified user does not exist"
