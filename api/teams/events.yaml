get:
  tags:
    - teams
  security:
    - AuthenticationToken: []
  summary: Get a team's events
  description: |
    Retrieve a list of event keys that the specified team is in.
  parameters:
    - in: path
      name: team
      schema:
        type: string
      required: true
      description: The ID of the team
  responses:
    200:
      description: Successfully retrieved team events
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                description: Success status message (will always be "success")
                example: success
              data:
                type: array
                description: The keys of the events the team is in
                items:
                  type: string
                  example: 2020caln
    400:
      description: Invalid request headers, body, or parameters
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                description: Error status message (will always be "error")
                example: error
              reason:
                type: string
                description: Human readable reason for failure
                example: invalid team id format for path parameter 'team'
    401:
      description: Invalid API token
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                description: Error status message (will always be "error")
                example: error
              reason:
                type: string
                description: Human readable reason for failure
                example: "invalid token: failed to find signing key for token"
    403:
      description: Not allowed to access resource
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                description: Error status message (will always be "error")
                example: error
              reason:
                type: string
                description: Human readable reason for failure
                example: "user not permitted to access route with given method"
    404:
      description: Specified team does not exist
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                description: Error status message (will always be "error")
                example: error
              reason:
                type: string
                description: Human readable reason for failure
                example: "specified team does not exist"
