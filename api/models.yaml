success:
  type: object
  properties:
    status:
      type: string
      description: The successful status message (will always be "success")
      example: success

team:
  type: object
  description: Information describing a team
  properties:
    id:
      type: string
      description: ID of the user in the database
      example: 5eb6526c55c384d04c0fa7f5
    name:
      type: string
      description: Name of the team
      example: WildHats
    number:
      type: integer
      description: The team's official number
      example: 100
    city:
      type: string
      description: The city where the team's from
      example: Woodside
    country:
      type: string
      description: The country where the team's from
      example: United States
    tba_url:
      type: string
      description: URL on the Blue Alliance
      example: https://thebluealliance.com/team/100
    trueskill:
      type: object
      description: The team's global trueskill representation
      properties:
        mu:
          type: number
          description: The mean of the team's skill
          example: 25
        sigma:
          type: number
          description: The standard deviation above or below integer
          example: 8.333

user:
  type: object
  description: User login information and personal information
  properties:
    id:
      type: string
      description: ID of the user in the database
      example: 5eb6526c55c384d04c0fa7f5
    name:
      type: string
      description: The user's full name
      example: Alex Krantz
    email:
      type: string
      description: The user's email
      example: alex@krantz.dev
    role:
      type: string
      description: The user's access permission level
      example: mentor
    enabled:
      type: bool
      description: Whether or not the user can login
      example: true
    email_verified:
      type: bool
      description: Whether or not the user's email was verified
      example: true

token:
  type: object
  description: A user's authentication token
  properties:
    id:
      type: string
      description: ID of the token in the database
      example: 5eb6526c55c384d04c0fa7f5
    os:
      type: string
      description: OS of the token requester
      example: Ubuntu
    browser:
      type: string
      description: Browser type and version of the token requester
      example: Firefox 76.0
    issued_at:
      type: integer
      description: Unix timestamp of when the token was issued
      example: 1589049760

event:
  type: object
  description: The event data retrieved from The Blue Alliance
  properties:
    id:
      type: string
      description: ID of the event in the database
      example: 5eb6526c55c384d04c0fa7f5
    name:
      type: string
      description: The human-readable name of the event
      example: San Francisco Regional
    key:
      type: string
      description: The key for the event on The Blue Alliance
      example: 2020casf
    start:
      type: integer
      description: Unix timestamp of when the event starts
      example: 1589049760
    end:
      type: integer
      description: Unix timestamp of when the event ends
      example: 1589049760
    year:
      type: integer
      description: The year in which the event occurs
      example: 2020
    city:
      type: string
      description: The city in which the event takes place
      example: San Francisco
    country:
      type: string
      description: The country where the event takes place
      example: United States
    state_province:
      type: string
      description: The state/province where the event takes place
      example: California

match:
  type: object
  description: The match data retrieved from The Blue Alliance
  properties:
    id:
      type: string
      description: ID of the match in the database
      example: 5eb6526c55c384d04c0fa7f5
    event:
      type: string
      description: The parent event ID in the database
      example: 5eb6526c55c384d04c0fa7f5
    key:
      type: string
      description: The key for the event on The Blue Alliance
      example: 2020casf_qm1
    scheduled_at:
      type: number
      description: When the match is scheduled to occur at
      example: 1589049760
    happened_at:
      type: number
      description: When the match actually happened
      example: 1589049760
    match_type:
      type: number
      description: The type of the match being played. 0 => qualification, 1 => quarter final, 2 => semi final, 3 => final
      enum:
        - 0
        - 1
        - 2
        - 3
      example: 1
    match_set_number:
      type: number
      description: The set number in a series of matches where more than one match is required in the match series
      example: 1
    match_number:
      type: number
      description: The match number of the match in the competition level
      example: 5
    winner:
      type: number
      description: The winner of the match (if it was played). 0 => unknown/not completed, 1 => red, 2 => blue
      example: 1
      enum:
        - 0
        - 1
        - 2
    red_score:
      type: number
      description: The red alliance's final score, will be zero if not yet played
      example: 52
    red_alliance:
      $ref: '#/team_alliance'
    blue_score:
      type: number
      description: The blue alliance's final score, will be zero if not yet played
      example: 28
    blue_alliance:
      $ref: '#/team_alliance'

team_alliance:
  type: object
  description: The teams within an alliance and their scoring breakdown
  properties:
    team_1:
      type: string
      description: The key of the team in the alliance
      example: frc100
    team_2:
      type: string
      description: The key of the team in the alliance
      example: frc971
    team_3:
      type: string
      description: The key of the team in the alliance
      example: frc1323
    score_breakdown:
      $ref: '#/score_breakdown'

score_breakdown:
  type: object
  description: The where each point was scored for an alliance
  properties:
    init_line_robot:
      type: array
      description: Whether or not each robot left the init line during autonomous. The length will always be 3
      items:
        type: number
        description: The status of the robot. 0 => unknown, 1 => did not leave, 2 => left
        enum:
          - 0
          - 1
          - 2
    endgame_robot:
      type: array
      description: The end state of each robot. The length will always be 3
      items:
        type: number
        description: The status of the robot. 0 => unknown, 1 => no where special, 2 => under the sheild generator, 3 => hanging
        enum:
          - 0
          - 1
          - 2
          - 3
    auto_cells_bottom:
      type: number
      description: The number of power cells scored in the bottom port during autonomous
      example: 5
    auto_cells_outer:
      type: number
      description: The number of power cells stored in the outer port during autonomous
      example: 2
    auto_cells_inner:
      type: number
      description: The number of power cells scored in the inner port during autonomous
      example: 4
    teleop_cells_bottom:
      type: number
      description: The number of power cells scored in the bottom port during teleop
      example: 21
    teleop_cells_outer:
      type: number
      description: The number of power cells scored in the outer port during teleop
      example: 30
    teleop_cells_inner:
      type: number
      description: The number of power cells scored in the inner port during teleop
      example: 14
    stage_1:
      type: bool
      description: Whether or not stage 1 is activated
      example: true
    stage_2:
      type: bool
      description: Whether or not stage 2 is activated
      example: false
    stage_3:
      type: bool
      description: Whether or not stage 3 is activated
      example: true
    endgame_rung_level:
      type: number
      description: The status of the rung at the end of the match. 0 => unknown, 1 => not level, 2 => level
      example: 1
      enum:
        - 0
        - 1
        - 2
    auto_init_line_points:
      type: number
      description: The number of points from teams leaving the init line
      example: 15
    auto_cell_points:
      type: number
      description: The number of points from all power cells scored during autonomous
      example: 40
    teleop_cell_points:
      type: number
      description: The number of points from all power cells scored during teleop
      example: 150
    control_panel_points:
      type: number
      description: The number of points from spinning the control panel
      example: 20
    auto_points:
      type: number
      description: The total points earned in autonomous
      example: 100
    teleop_points:
      type: number
      description: The total points earned in teleop
      example: 200
    endgame_points:
      type: number
      description: The total points earned during the endgame
      example: 75
    adjust_points:
      type: number
      description: The total points given to adjust the alliance's score
      example: 0
    foul_points:
      type: number
      description: The total points earned from fouls
      example: 5
    total_points:
      type: number
      description: The total number of points earned during the match
      example: 300
    shield_operational_ranking_point:
      type: bool
      description: Whether or not the sheild operational ranking point was earned
      example: false
    shield_energized_ranking_point:
      type: bool
      description: Whether or not the shield energized ranking point was earned
      example: true
    ranking_points:
      type: number
      description: The number of ranking points earned from the match
      example: 2
    foul_count:
      type: number
      description: The number of fouls commited by the alliance
      example: 1
    tech_foul_count:
      type: number
      description: The number of technical fouls committed by the alliance
      example: 1

event_ranking:
  type: object
  description: An official team ranking for an event
  properties:
    event:
      type: string
      description: The event's key from The Blue Alliance
      example: 2020casf
    team:
      type: string
      description: The team's key from The Blue Alliance
      example: frc100
    rank:
      type: number
      description: The rank of a team in the event
      example: 2
    matches_played:
      type: number
      description: The total number of matches played in an event
      example: 5
    disqualifications:
      type: number
      description: The total number of disqualifications in an event
      example: 0
    total_ranking_points:
      type: number
      description: The total number of ranking points earned
      example: 5
    ranking_score:
      type: number
      description: The average ranking points earned per match
      example: 2.33

alliance:
  type: object
  description: An alliance during the quarter-/semi-/finals
  properties:
    name:
      type: string
      description: The name of the alliance
      example: Alliance 1
    teams:
      type: array
      description: The keys of the teams in an alliance
      items:
        type: string
        example: frc100
    status:
      type: string
      description: Whether or not the team is eliminated or won
      example: won
      enum:
        - won
        - eliminated
    wins:
      type: number
      description: The number of matches won
      example: 5
    ties:
      type: number
      description: The number of matches tied
      example: 0
    losses:
      type: number
      description: The number of matches loss
      example: 1

event_match_average:
  type: object
  description: Average data from scouting matches for a team over an event
  properties:
    team:
      type: number
      description: The team's number
      example: 100
    event:
      type: string
      description: The event key from The Blue Alliance
      example: 2020casf
    percent_malfunction:
      type: number
      description: The percent of matches a team has malfunctioned for
      example: 0.25
    push_elo:
      type: number
      description: The ELO ranking for a team's push ability
      example: 1024
    data_points:
      type: number
      description: The number of times data was gathered from matches for a team
      example: 5
    matches:
      type: number
      description: The number of matches played
      example: 3
    scouted_by:
      type: string
      description: The ID of the team that generated the data
      example: 5eb6526c55c384d04c0fa7f5
    average_total_shots:
      type: number
      description: The average shots taken across all targets
      example: 2.5
    average_low_shots:
      type: number
      description: The average shots taken in the low goal
      example: 1.2
    average_outer_shots:
      type: number
      description: The average shots taken in the outer goal
      example: 4.2
    average_inner_shots:
      type: number
      description: The average shots taken in the inner goal
      example: 1.4
    average_cumulative_accuracy:
      type: number
      description: The average accuracy across all targets during all time-periods
      example: 0.23
    average_auto_accuracy:
      type: number
      description: The average accuracy across all targets during autonomous
      example: 0.27
    average_teleop_accuracy:
      type: number
      description: The average accuracy across all targets during teleop
      example: 0.72
    average_low_accuracy_auto:
      type: number
      description: The average accuracy for the low goal during autonomous
      example: 0.27
    average_low_accuracy_teleop:
      type: number
      description: The average accuracy for the low goal during teleop
      example: 0.73
    average_low_accuracy_cumulative:
      type: number
      description: The average accuracy for the low goal across all time-periods
      example: 0.62
    average_outer_accuracy_auto:
      type: number
      description: The average accuracy for the outer goal during autonomous
      example: 0.27
    average_outer_accuracy_teleop:
      type: number
      description: The average accuracy for the outer goal during teleop
      example: 0.73
    average_outer_accuracy_cumulative:
      type: number
      description: The average accuracy for the outer goal across all time-periods
      example: 0.62
    average_inner_accuracy_auto:
      type: number
      description: The average accuracy for the inner goal during autonomous
      example: 0.27
    average_inner_accuracy_teleop:
      type: number
      description: The average accuracy for the inner goal during teleop
      example: 0.73
    average_inner_accuracy_cumulative:
      type: number
      description: The average accuracy for the inner goal across all time-periods
      example: 0.62
    average_low_cycle_times:
      type: number
      description: The average cycle time for the low goal in seconds
      example: 2.5
    average_outer_cycle_times:
      type: number
      description: The average cycle time for the outer goal in seconds
      example: 2.5
    average_inner_cycle_times:
      type: number
      description: The average cycle time for the inner goal in seconds
      example: 2.5
    average_collection_accuracy:
      type: number
      description: The average success rate for collecting power cells
      example: 0.5
    average_collected:
      type: number
      description: The average number of power cells collected per match
      example: 5.2
    average_climb_time:
      type: number
      description: The average time it takes for the team to climb
      example: 5.2
    average_climb_accuracy:
      type: number
      description: The average success rate for climbing
      example: 0.2
    average_control_panel_stage_2_accuracy:
      type: number
      description: The average success rate for the control panel on stage 2
      example: 0.7
    average_control_panel_stage_3_accuracy:
      type: number
      description: The average success rate for the control panel on stage 3
      example: 0.2
    average_block_accuracy:
      type: number
      description: The average success rate on blocking another team
      example: 0.01
    average_blocks:
      type: number
      description: The average number of attempted blocks per match
      example: 1

team_match_data:
  type: object
  description: Scouted data for a match for a team
  properties:
    match:
      type: string
      description: The ID of the match the data is from
      example: 5eb6526c55c384d04c0fa7f5
    team:
      type: string
      description: The ID of the team the data is for
      example: 5eb6526c55c384d04c0fa7f5
    scouted_by:
      type: string
      description: The ID of the team that generated the data
      example: 5eb6526c55c384d04c0fa7f5
    total_shots:
      type: number
      description: The total number of shots taken across the entire match
      example: 50
    low_shots:
      type: number
      description: The total number of shots taken in the low goal
      example: 10
    outer_shots:
      type: number
      description: The total number of shots taken in the outer goal
      example: 10
    inner_shots:
      type: number
      description: The total number of shots taken in the inner goal
      example: 10
    total_shot_locations:
      type: object
      description: The total number of shots taken from various places and scored in any goal
      # TODO: add each specific location to object properties
    low_shot_locations:
      type: object
      description: The total number of shots taken per location to the low goal
      # TODO: add each specific location to object properties
    outer_shot_locations:
      type: object
      description: The total number of shots taken per location to the outer goal
      # TODO: add each specific location to object properties
    inner_shot_locations:
      type: object
      description: The total number of shots taken per location to the inner goal
      # TODO: add each specific location to object properties
    cumulative_accuracy:
      type: number
      description: The overall accuracy across all goals and time-periods
      example: 0.83
    auto_accuracy:
      type: number
      description: The overall accuracy across all goals during autonomous
      example: 0.98
    teleop_accuracy:
      type: number
      description: The overall accuracy across all goals during teleop
      example: 0.74
    low_accuracy_auto:
      type: number
      description: The accuracy for the low goal during autonomous
      example: 1.0
    low_accuracy_teleop:
      type: number
      description: The accuracy for the low goal during teleop
      example: 0.62
    low_accuracy_cumulative:
      type: number
      description: The overall accuracy for the low goal during all time-periods
      example: 0.82
    outer_accuracy_auto:
      type: number
      description: The accuracy for the outer goal during autonomous
      example: 1.0
    outer_accuracy_teleop:
      type: number
      description: The accuracy for the outer goal during teleop
      example: 0.62
    outer_accuracy_cumulative:
      type: number
      description: The overall accuracy for the outer goal during all time-periods
      example: 0.82
    inner_accuracy_auto:
      type: number
      description: The accuracy for the inner goal during autonomous
      example: 1.0
    inner_accuracy_teleop:
      type: number
      description: The accuracy for the inner goal during teleop
      example: 0.62
    inner_accuracy_cumulative:
      type: number
      description: The overall accuracy for the inner goal during all time-periods
      example: 0.82
    low_cycle_times:
      type: array
      description: All cycle times for the low goal
      items:
        type: number
    low_cycle_times_average:
      type: number
      description: The average cycle time for the low goal
      example: 7.8
    outer_cycle_times:
      type: array
      description: All cycle times for the outer goal
      items:
        type: number
    outer_cycle_times_average:
      type: number
      description: The average cycle time for the outer goal
      example: 7.8
    inner_cycle_times:
      type: array
      description: All cycle times for the inner goal
      items:
        type: number
    inner_cycle_times_average:
      type: number
      description: The average cycle time for the inner goal
      example: 7.8
    control_panel_stage_2_accuracy:
      type: number
      description: The accuracy for the control panel on stage 2
      example: 1.0
    control_panel_stage_2_duration:
      type: number
      description: The time taken for control panel on stage 2
      example: 7.2
    control_panel_stage_2_successful:
      type: bool
      description: Whether the control panel was successful on stage 2
      example: true
    control_panel_stage_3_accuracy:
      type: number
      description: The accuracy for the control panel on stage 3
      example: 1.0
    control_panel_stage_3_duration:
      type: number
      description: The time taken for control panel on stage 3
      example: 7.2
    control_panel_stage_3_successful:
      type: bool
      description: Whether the control panel was successful on stage 3
      example: true
    total_blocks:
      type: number
      description: The total number of times the team successfully blocked someone
      example: 2
    block_accuracy:
      type: number
      description: The accuracy of the team for blocking
      example: 0.5
    total_malfunctions:
      type: number
      description: The total number of times the team malfunctioned
      example: 1
    total_malfunction_time:
      type: number
      description: The total time the team was malfunctioning for
      example: 5
    malfunctions:
      type: object
      description: The total malfunctions and the times each one took
      properties:
        "0":
          type: array
          description: The time the team's robot was broken
          items:
            type: number
        "1":
          type: array
          description: The time the team's robot was browned out
          items:
            type: number
        "2":
          type: array
          description: The time the team's robot was disabled
          items:
            type: number
        "3":
          type: array
          description: The time the team's robot had some other type of malfunction
          items:
            type: number
    climb_time:
      type: number
      description: The total time it took to climb
      example: 10
    climb_accuracy:
      type: number
      description: The accuracy of the team's climbing ability
      example: 0.6
    climb_successful:
      type: number
      description: Whether or not they successfully climbed
      example: true
    total_collected:
      type: number
      description: The total number of power cells collected
      example: 52
    collection_accuracy:
      type: number
      description: How accurate the team was at collecting power cells
      example: 0.8
    collection_locations:
      type: object
      description: The distribution of where power cells were collected
      properties:
        "0":
          type: number
          description: Power cells collected from the friendly loading zone
          example: 32
        "1":
          type: number
          description: Power cells collected from the opponent loading zone
          example: 12
        "2":
          type: number
          description: Power cells collected from the friendly trench
          example: 23
        "3":
          type: number
          description: Power cells collected from the opponent trench
          example: 16
        "4":
          type: number
          description: Power cells collected from the friendly boundaries
          example: 30
        "5":
          type: number
          description: Power cells collected from the opponent boundaries
          example: 16
        "6":
          type: number
          description: Power cells collected from the friendly sector
          example: 24
        "7":
          type: number
          description: Power cells collected from the opponent sector
          example: 24
        "8":
          type: number
          description: Power cells collected from somewhere else
          example: 0
    total_push_matches:
      type: number
      description: The number of push matches the team got into
      example: 2
    total_push_wins:
      type: number
      description: The number of push matches won
      example: 1
    total_push_time:
      type: number
      description: The total time push matches took
      example: 6.4

pit_scout:
  type: object
  description: Information for pit scouting
  properties:
    weight:
      type: number
      description: The weight of the robot
      example: 112.5
    width:
      type: number
      description: The width of the robot
      example: 32.2
    length:
      type: number
      description: The length of the robot
      example: 37.2
    drive_train_motor_count:
      type: number
      description: The number of motors on the drive train
      example: 6
    drive_train_motor_type:
      type: string
      description: The motors being used on the drive train
      enum:
        - Mini CIM
        - CIM
        - Neo
        - Falcon
      example: Mini CIM
    drive_train_type:
      type: string
      description: The type of drive train being used
      enum:
        - Tank
        - Swerve
        - Mecanum
        - Tank Treads
      example: Tank
    wheel_type:
      type: string
      description: The wheels being used on the drive train
      enum:
        - Traction
        - Colson
        - Pneumatic
        - Omni
      example: Traction
    wheel_diameter:
      type: number
      description: The diameter of the wheels on the drive train
      enum:
        - 4
        - 6
        - 8
      example: 6
    programming_language:
      type: string
      description: The programming language used on the robot
      enum:
        - Java
        - C++
        - LabView
      example: Java
    camera:
      type: bool
      description: Whether or not the team has a camera
      example: true
    notes:
      type: string
      description: Other notes about the team
      example: here are some notes
    game_specific:
      $ref: '#/game_specific'
    completed:
      type: bool
      description: Whether or not all information for pit scouting has been filled in
      example: true

game_specific:
  type: object
  description: The game-specific data for pit scouting
  properties:
    climbing_method:
      type: string
      description: The method used for climbing
      example: Hook
    move_on_bar:
      type: bool
      description: Whether or not the team is able to climb on the bar
      example: false
    carry_robot:
      type: bool
      description: Whether or not the team can carry another robot on the bar
      example: false
    low_goal:
      type: bool
      description: Whether or not the team can score in the low goal
      example: true
    outer_goal:
      type: bool
      description: Whether or not the team can score in the outer goal
      example: true
    inner_goal:
      type: bool
      description: Whether or not the team can score in the inner goal
      example: false
